const symbolcroix = '<img src="../img/croix.png">';
const symbolrond = '<img src="../img/rond.png">';
const player1 = {symbole: symbolcroix, name: sessionStorage.getItem("username1")};
const player2 = {symbole: symbolrond, name: sessionStorage.getItem("username2")};
let currentPlayer = player1;
let aQuiLeTour = 0;
let clicked = false;
let win = false;

let jeuActif = true;
let nombreDeTour = 0;
let stopGame = false;
const timerLimit = 0;
let timer = 20;
let counterTurn = 0;
let counterVal = 1;
let partieTerminee = false;
let chrono;

function createTable(taille) {
	table = "";
	for (var tempX = 0; tempX < taille; tempX++) {
		table = table + createLigne(tempX, taille);
	}
	document.getElementById("morpion-table").innerHTML = table;
}

function createLigne(numLigne, taille) {
	cellule = "";
	for(var tempY = 0; tempY < taille; tempY++) {
		cellule = cellule + '<td><button onclick="Click(' +numLigne + "," + tempY +')" id="'+ createID(numLigne, tempY) +'"></button></td>';
	}
	return "<tr>" + cellule + "</tr>";
}

function createID(x, y){
	return "cell-" + x + "-" + y;
}


var boardGame = initBoardGame(3);
	console.log(boardGame);

function initBoardGame(taille){
	var boardGame = [];
	for(var tempX=0;tempX<taille;tempX++){
	boardGame.push([]);
		for(var tempY=0;tempY<taille;tempY++){
		boardGame[tempX].push("vide");
		}
	}
	return boardGame;
}


function Click(x, y) {
	if(partieTerminee) return;
	document.getElementById("cell-" + x + "-" + y).disabled = true;
	timer = 20;
	console.log("counterTurn", ++ counterTurn);
	console.log(boardGame);
	Tour(x, y);
	changeBoardGame (x, y, currentPlayer.name);
	calculWinner();
	isDraw();
}


function changeBoardGame(x, y, namePlayer) {
	boardGame[x][y] = namePlayer;
}


function Tour(x, y) {
	if(calculWinner() || isDraw()){
		return;
	}
	aQuiLeTour ++; 
	currentPlayer = aQuiLeTour %2 == 1 ? player1:player2;
	setTextInHtml("joueurX", "C'est au tour de " + currentPlayer.name);
	document.getElementById("cell-" + x + "-" + y).innerHTML = currentPlayer.symbole;

	nombreDeTour = nombreDeTour + 1;
}


function progressBarTimer() {
	document.getElementById("div-timer-bar").style.width = timer * 5 + "%";
  }
  
  function resetTimer() {
	chrono = setInterval(() => {
	  decrementeTimer();
	  if (calculWinner() | isDraw()) {
		clearInterval(chrono);
	  }
	}, 1000);
  }
 
  
  function decrementeTimer() {
	console.log("decremente", timer);
	progressBarTimer();
	if (timer > 0) {
	  timer--;
	} else {
	  lost();
	}
  }
  
resetTimer();


function calculWinner() {
	var c = boardGame;
	let tempWin = false;

	// if(c[0][0] == c[0][1] && c[0][0] == c[0][2] && c[0][0] != "vide" && !!c[0][0]) {
	// 	tempWin= true;
	// } else if (c[1][0] == c[1][1] && c[1][0] == c[1][2] && c[1][0] != "vide" && !!c[1][0]) {
	// 	tempWin= true;
	// } else if (c[2][0] == c[2][1] && c[2][0] == c[2][2] && c[2][0] != "vide" && !!c[2][0]) {
	// 	tempWin= true;
	// } else if (c[0][0] == c[1][0] && c[0][0] == c[2][0] && c[0][0] != "vide" && !!c[0][0]) {
	// 	tempWin= true;
	// } else if (c[0][1] == c[1][1] && c[0][1] == c[2][1] && c[0][1] != "vide" && !!c[0][1]) {
	// 	tempWin= true;
	// } else if (c[0][2] == c[1][2] && c[0][2] == c[2][2] && c[0][2] != "vide" && !!c[0][2]) {
	// 	tempWin= true;
	// } else if (c[0][0] == c[1][1] && c[0][0] == c[2][2] && c[0][0] != "vide" && !!c[0][0]) {
	// 	tempWin= true;
	// } else if (c[0][2] == c[1][1] && c[0][2] == c[2][0] && c[0][2] != "vide" && !!c[0][2]) {
	// 	tempWin= true;
	//	}

	if (c[0][0] == c[0][1] && c[0][0] == c[0][2] && c[0][0] != "vide" && !!c[0][0]) tempWin= true;
	if (c[1][0] == c[1][1] && c[1][0] == c[1][2] && c[1][0] != "vide" && !!c[1][0]) tempWin= true;
	if (c[2][0] == c[2][1] && c[2][0] == c[2][2] && c[2][0] != "vide" && !!c[2][0]) tempWin= true;
	if (c[0][0] == c[1][0] && c[0][0] == c[2][0] && c[0][0] != "vide" && !!c[0][0]) tempWin= true;
	if (c[0][1] == c[1][1] && c[0][1] == c[2][1] && c[0][1] != "vide" && !!c[0][1]) tempWin= true;
	if (c[0][2] == c[1][2] && c[0][2] == c[2][2] && c[0][2] != "vide" && !!c[0][2]) tempWin= true;
	if (c[0][0] == c[1][1] && c[0][0] == c[2][2] && c[0][0] != "vide" && !!c[0][0]) tempWin= true;
	if (c[0][2] == c[1][1] && c[0][2] == c[2][0] && c[0][2] != "vide" && !!c[0][2]) tempWin= true;
	win = tempWin;

	if(win) {
		displayWin();
		partieTerminee = true;
	}
	return win;
}

function setTextInHtml(id, text) {
	var changeText = document.getElementById(id);
	changeText.innerText = text;
}

function displayWin(){
    setTextInHtml("joueurX","Bravo " + currentPlayer.name + " ! " + " Vous avez gagnez !");
    setTextInHtml("result", "Résultat du match : " + "" + "vainqueur : " + "" + currentPlayer.name);
}

function lost() {
	setTextInHtml("joueurX", "Temps écoulé. C'est perdu !");
	partieTerminee = true;
}

function isDraw() {
	var verifEgalite =
	  !boardGame.filter(
		(ligne) => ligne.filter((cellule) => cellule === "vide").length > 0
	  ).length > 0;
  
	if (verifEgalite == true) {
	  setTextInHtml("joueurX", "Match Nul !");
	  partieTerminee = true;
	  setTextInHtml("result", "Résultat du match : " + "Match nul");
	}
	return verifEgalite;
}


function myFunction() {
	setTextInHtml("nombre-turn", counterTurn);
	document.body.classList.toggle("background-when-popup");
	var popupElement = document.getElementById("myPopup");
	popupElement.classList.toggle("show");
}